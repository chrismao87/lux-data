package provider

import (
	"context"
	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/alpacahq/alpaca-trade-api-go/common"
	"github.com/shopspring/decimal"
	"os"
)

type AlpacaDataService struct {
	client *alpaca.Client
}

func NewAlpacaDataService(apiKey, apiSecret, baseUrl string) *AlpacaDataService {
	err := os.Setenv(common.EnvApiKeyID, apiKey)
	err = os.Setenv(common.EnvApiSecretKey, apiSecret)
	if err != nil {
		return nil
	}
	alpaca.SetBaseUrl(baseUrl)
	return &AlpacaDataService{
		client: alpaca.NewClient(common.Credentials()),
	}
}

func (a *AlpacaDataService) SubscribeMarketData(opt SubscribeDataOption, update IMarketDataUpdate) {
	panic("implement me")
}

func (a *AlpacaDataService) UnsubscribeMarketData(opt SubscribeDataOption) {
	panic("implement me")
}

func (a *AlpacaDataService) GetPrice(ctx context.Context, symbol string) (string, error) {
	trade, err := a.client.GetLatestTrade(symbol)
	if err != nil {
		return "", err
	}
	return decimal.NewFromFloat(trade.Price).String(), nil
}
