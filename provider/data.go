package provider

import "context"

type MarketDataProviderType string

const (
	MarketDataProviderTypeBinance MarketDataProviderType = "Binance"
	MarketDataProviderTypeAlpaca  MarketDataProviderType = "Alpaca"
)

type IMarketDataUpdate interface {
	Update(data *MarketData)
}

type MarketDataUpdateFunc func(data *MarketData)

func (fn MarketDataUpdateFunc) Update(data *MarketData) {
	fn(data)
}

type SubscribeDataOption struct {
	Symbol string
}

type IData interface {
	SubscribeMarketData(opt SubscribeDataOption, update IMarketDataUpdate)

	UnsubscribeMarketData(opt SubscribeDataOption)

	GetPrice(ctx context.Context, symbol string) (string, error)
}

type MarketData struct {
	Symbol string
	Bid    string
	Ask    string
	Last   string
	BidQty string
	AskQty string
	Time   int64
}

type MarketDataSubscribeInfo struct {
	symbol   string
	provider MarketDataProviderType
}
