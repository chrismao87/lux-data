package provider

import (
	"context"
	"errors"
	"github.com/adshao/go-binance/v2"
	"go.mongodb.org/mongo-driver/mongo"
	"sync"
)

const (
	websocketServiceRestartCoolDownMinute = 5
)

type BinanceDataService struct {
	client  *binance.Client
	mongodb *mongo.Database
	mu      sync.Mutex
}

func (b *BinanceDataService) GetPrice(ctx context.Context, symbol string) (p string, err error) {
	res, err := b.client.NewListPricesService().Symbol(symbol).Do(ctx)
	if err != nil {
		return
	}
	for _, pair := range res {
		if pair.Symbol == symbol {
			p = pair.Price
			return
		}
	}
	err = errors.New("price not found")
	return
}

//func (b *BinanceDataService) StartService(ctx context.Context) {
//	errorC := make(chan struct{})
//	defer close(errorC)
//
//	c := b.mongodb.Collection("market_data_sub")
//
//	var stopC chan struct{}
//	fn := func() {
//		wsHandler := func(event *provider.WsMarketStatEvent) {
//			update.Update(&data.MarketData{
//				Symbol: event.Symbol,
//				Bid:    event.BidPrice,
//				Ask:    event.AskPrice,
//				Last:   event.LastPrice,
//				BidQty: event.BidQty,
//				AskQty: event.AskQty,
//				Time:   event.Time,
//			})
//		}
//		errHandler := func(err error) {
//			log.WithField("error", err).Error("error on WsMarketStatServe")
//			errorC <- struct{}{}
//		}
//		b.mu.Lock()
//		var err error
//		_, stopC, err = provider.WsMarketStatServe(opt.Symbol, wsHandler, errHandler)
//		if err != nil {
//			log.WithField("error", err).Error("error on starting WsMarketStatServe")
//			return
//		}
//	}
//
//	go func() {
//		wsHandler := func(event *provider.WsAllMarketsStatEvent) {
//
//		}
//		provider.WsAllMarketsStatHandler(func(event provider.WsAllMarketsStatEvent) {
//
//		})
//		provider.WsAllMarketsStatServe()
//	}()
//
//	fn()
//
//	for {
//		select {
//		case <-errorC:
//			go fn()
//			<-time.After(time.Minute * websocketServiceRestartCoolDownMinute)
//		}
//	}
//}

func (b *BinanceDataService) SubscribeMarketData(opt SubscribeDataOption, update IMarketDataUpdate) {
	panic("implement me")
}

func (b *BinanceDataService) UnsubscribeMarketData(opt SubscribeDataOption) {
	panic("implement me")
}

func NewBinanceDataService(apiKey, apiSecret string, database *mongo.Database) *BinanceDataService {
	return &BinanceDataService{
		client:  binance.NewClient(apiKey, apiSecret),
		mongodb: database,
	}
}
