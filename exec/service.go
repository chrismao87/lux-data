package main

import (
	"context"
	"gitlab.com/chrismao87/lux-data/provider"
	"gitlab.com/chrismao87/lux-data/public"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type server struct {
	data                  provider.IData
	marketUpdateHandlerFn provider.MarketDataUpdateFunc
	public.UnimplementedMarketDataServer
}

func (s *server) GetPrice(ctx context.Context, request *public.GetPriceRequest) (*public.GetPriceResponse, error) {
	p, err := s.data.GetPrice(ctx, request.GetSymbol())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &public.GetPriceResponse{Price: p}, nil
}

func (s *server) SubscribeMarketData(ctx context.Context, request *public.SubscribeMarketDataRequest) (*public.SubscribeMarketDataResponse, error) {
	s.data.SubscribeMarketData(provider.SubscribeDataOption{Symbol: request.GetSymbol()}, s.marketUpdateHandlerFn)
	return &public.SubscribeMarketDataResponse{}, nil
}

func (s *server) UnsubscribeMarketData(ctx context.Context, request *public.UnsubscribeMarketDataRequest) (*public.UnsubscribeMarketDataResponse, error) {
	s.data.UnsubscribeMarketData(provider.SubscribeDataOption{Symbol: request.GetSymbol()})
	return &public.UnsubscribeMarketDataResponse{}, nil
}
