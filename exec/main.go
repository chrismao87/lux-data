package main

import (
	"context"
	"flag"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/chrismao87/lux-data/provider"
	"gitlab.com/chrismao87/lux-data/public"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	port                      = ":50051"
	DataProviderName          = "DATA_PROVIDER_NAME"
	DataProviderApiKey        = "DATA_PROVIDER_API_KEY"
	DataProviderApiSecret     = "DATA_PROVIDER_API_SECRET"
	MarketDataUpdateQueueName = "MARKET_DATA_UPDATE_QUEUE_NAME"
	DefaultMarkString         = "*********"
)

func init() {

	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{TimestampFormat: "2006-01-02 15:04:05.000"})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	//You could set this to any `io.Writer` such as a file
	//file, err := os.OpenFile("/Users/chrismao/Downloads/marin.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	//if err == nil {
	//	log.SetOutput(file)
	//} else {
	//	log.Error("Failed to log to file, using default stderr")
	//}

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)

	//log.SetReportCaller(true)
}

type InputVars struct {
	dataProviderVar        *string
	apiKeyVar              *string
	apiSecretVar           *string
	marketDataQueueNameVar *string
}

func GetInputVars() InputVars {
	dataProviderVar := flag.String("provider_name", "", "the specific provider for this instance. e.g. provider")
	apiKey := flag.String("api_key", "", "api key for the provider API")
	apiSecret := flag.String("api_secret", "", "api secret for the provider API")
	marketDataQueueName := flag.String("market_data_queue_name", "", "message queue name where market data send to")

	flag.Parse()

	inputVars := InputVars{
		dataProviderVar:        dataProviderVar,
		apiKeyVar:              apiKey,
		apiSecretVar:           apiSecret,
		marketDataQueueNameVar: marketDataQueueName,
	}

	viper.SetEnvPrefix("LUX")
	overrideFromEnv := func(input *string, key string, mask bool) {
		_ = viper.BindEnv(key)
		oldVal := *input
		*input = viper.GetString(key)
		newVal := *input
		if mask {
			oldVal = DefaultMarkString
			newVal = DefaultMarkString
		}
		log.WithFields(log.Fields{
			"key": key,
			"old": oldVal,
			"new": newVal,
		}).Info("override flag with env var")
	}
	if *inputVars.dataProviderVar == "" {
		overrideFromEnv(inputVars.dataProviderVar, DataProviderName, false)
	}
	if *inputVars.apiKeyVar == "" {
		overrideFromEnv(inputVars.apiKeyVar, DataProviderApiKey, false)
	}
	if *inputVars.apiSecretVar == "" {
		overrideFromEnv(inputVars.apiSecretVar, DataProviderApiSecret, true)
	}
	if *inputVars.marketDataQueueNameVar == "" {
		overrideFromEnv(inputVars.marketDataQueueNameVar, MarketDataUpdateQueueName, false)
	}
	return inputVars
}

func main() {
	inputVars := GetInputVars()
	logger := log.WithFields(log.Fields{
		"dataProvider":            inputVars.dataProviderVar,
		"marketDataQueueName": inputVars.marketDataQueueNameVar,
	})

	lis, err := net.Listen("tcp", port)
	if err != nil {
		logger.WithError(err).Fatal("failed to listen")
	}
	var p provider.IData
	switch *inputVars.dataProviderVar {
	case "binance":
		p = provider.NewBinanceDataService(*inputVars.apiKeyVar, *inputVars.apiSecretVar, nil)
	case "alpaca":
		p = provider.NewAlpacaDataService(*inputVars.apiKeyVar, *inputVars.apiSecretVar, "https://paper-api.alpaca.markets")
	}
	g := grpc.NewServer()
	public.RegisterMarketDataServer(g, &server{data: p})
	reflection.Register(g)
	go func() {
		logger.WithField("port", port).Info("start grpc server")
		if err := g.Serve(lis); err != nil {
			logger.WithError(err).Fatal("failed to serve")
		}
	}()

	WaitForShutdown(func() {
		g.GracefulStop()
	})
}

func WaitForShutdown(doBeforeShutdown func()) {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(
		signalChan,
		syscall.SIGHUP,  // kill -SIGHUP XXXX
		syscall.SIGINT,  // kill -SIGINT XXXX or Ctrl+c
		syscall.SIGTERM, // kill -SIGTERM XXXX
		syscall.SIGQUIT, // kill -SIGQUIT XXXX
	)
	<-signalChan
	log.Info("os.Interrupt - shutting down...")

	exitCtx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	go func() {
		defer cancel()
		doBeforeShutdown()
	}()

	<-exitCtx.Done()
	log.Info("gracefully cleaned up, exit")
}
