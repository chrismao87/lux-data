module gitlab.com/chrismao87/lux-data

go 1.16

require (
	github.com/adshao/go-binance/v2 v2.2.2
	github.com/alpacahq/alpaca-trade-api-go v1.8.2
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.5.3
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.25.0
)
